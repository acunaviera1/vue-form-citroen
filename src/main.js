import Vue from 'vue'
import vSelect from 'vue-select'
import VeeValidate from 'vee-validate'
import {
  rutInputDirective
} from 'vue-dni';


import App from './App.vue'
import store from './store'

Vue.config.productionTip = true
Vue.component('v-select', vSelect)
Vue.use(VeeValidate)
Vue.directive('rut', rutInputDirective)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
